//import {useState, useEffect} from 'react'; //use to 
import {Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard ({courseProp}) {
//prop is from courseCard
//prop is used to 
	//console.log(props)
	//console.log(typeof props)
	//use {courseProp} to destructure: from props argument to {courseProp}
	//instead of {props.courseProp.name}, just remove props

	//console.log(courseProp)
	//destructure of object - is use for code readability
	//onclick on js lowercase, in react it uses camelcase

	 const { name, description, price, _id} = courseProp
	 //console.log(courseProp) 


	// /*
	// 	Syntax: 
	// 		const [getter, setter] = useState(initialGetterValue)
	// */
	// const [count, setCount] = useState(0)
	// // Use state hook for getting and setting the seats for this course
	// const [seats, setSeats] = useState(30)

	// function enroll () {

	
	// 		setCount(count +1);
	// 		console.log('Enrollees' + count);
	// 		setSeats(seats -1);
	// 		console.log('Enrollees' + seats);
			
	// 	}

	// 	useEffect(() => {

	// 		if (seats === 0) {
	// 			alert("No more seats available.");
	// 		}

	// 	}, [seats]);//if no changes it wont trigger


		


	//console.log(props.courseProp.name) - to access the name from the prop
	//curly braces uses in props to signify usage of JS expression
	return (
		<Card>			  
				  <Card.Body className="mb-3">
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle>Description</Card.Subtitle>
				    <Card.Text>
				      {description}
				    </Card.Text>
				    <Card.Subtitle>Price:</Card.Subtitle>
				    <Card.Text>PhP {price}</Card.Text>
				   {/* <Card.Text>Enrollees: {count}</Card.Text>
				    <Card.Text>Seats: {seats}</Card.Text>
				    <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
				    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
				  </Card.Body>
				</Card>
			
		)

}