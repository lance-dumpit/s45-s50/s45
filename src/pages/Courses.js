import PropTypes from 'prop-types';
import {Fragment, useEffect, useState} from 'react'; 
import CourseCard from '../components/CourseCard';
//import coursesData from '../data/coursesData';


export default function Courses() {

	//console.log(coursesData)
	//{coursesData[0]} is the mock data which is laravel
	//courseProp is a property of courseCard

	//map method to loop courses
	//key is a string attribute bc there's a list of elements (should be referenced in the doc)
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })

	const [courses, setCourses] =useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/courses')
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setCourses(data.map(course => {
				return (

					<CourseCard key={course._id} courseProp={course} />

					)
			}))
		})
		
	}, [])

	return (
		<Fragment>
			{courses}
		</Fragment>		

		)
}

//Proptypes is used to check if the data type that is entered is correct
//shape method  - expectinf the shape / format of this object
CourseCard.propTypes = {

	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

